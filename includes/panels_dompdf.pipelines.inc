<?php

/**
 * Implements hook_default_panels_renderer_pipeline().
 */
function panels_dompdf_default_panels_renderer_pipeline() {
  $pipelines = array();

  $pipeline = new stdClass;
  $pipeline->disabled = FALSE; /* Edit this to true to make a default pipeline disabled initially */
  $pipeline->api_version = 1;
  $pipeline->name = 'dompdf';
  $pipeline->admin_title = 'Dompdf';
  $pipeline->admin_description = t('Renders a panel as PDF.');
  $pipeline->weight = 2;
  $pipeline->settings = array(
    'renderers' => array(
      0 => array(
        'access' => array(),
        'renderer' => 'dompdf',
        'options' => array(),
      ),
    ),
  );
  $pipelines[$pipeline->name] = $pipeline;

  return $pipelines;
}
