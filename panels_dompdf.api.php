<?php
/**
 * @file
 * API documentation for panels_dompdf.
 */

/**
 * Allows you to add content to the pdf canvas like performing drawings.
 */
function hook_panels_dompdf_canvas_alter(Canvas $canvas, $subtype, $vars){
  if($subtype == 'my_panels_machine_name'){
    $canvas->page_text(275, 810, "{PAGE_NUM} / {PAGE_COUNT}", "", 9);
  }
}
