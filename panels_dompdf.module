<?php
/**
 * @file
 * Useful functions and hook implementations for ECK.
 */

use Dompdf\Canvas;

/**
 * Implementation of hook_ctools_plugin_api().
 *
 * Inform CTools about version information for various plugins implemented by
 * Panels.
 *
 * @param string $owner
 *   The system name of the module owning the API about which information is
 *   being requested.
 * @param string $api
 *   The name of the API about which information is being requested.
 */
function panels_dompdf_ctools_plugin_api($owner, $api) {
  if ($owner == 'panels' && $api == 'pipelines') {
    return array(
      'version' => 1,
      'path' => drupal_get_path('module', 'panels_dompdf') . '/includes',
    );
  }
}

/**
 * Implementation of hook_ctools_plugin_directory().
 */
function panels_dompdf_ctools_plugin_directory($module, $plugin) {
  if ($module == 'panels' && $plugin == 'display_renderers') {
    return 'plugins/' . $plugin;
  }
}

function panels_dompdf_form_panels_panel_context_edit_settings_alter(&$form, $form_state){
  $display = &$form_state['display'];
  $settings = $form_state['handler']->conf + array(
    'filename' => 'Filename',
    'attachment' => 0,
    'debug_dompdf' => 0,
    'save_pdf' => 0,
    'save_pdf_path' => '',
    'save_html' => 0,
    'save_html_path' => '',
    'css_files' => '',
  );
  
  $conf = $form['conf'];
  $form['conf'] = array();
  foreach ($conf as $key => $value) {
    
    $form['conf'][$key] = $value;
    if($key == 'pipeline'){
      
      $form['conf']['dompdf'] = array(
        '#type' => 'fieldset',
        '#title' => 'Dompdf settings',
        '#states' => array(
          'visible' => array(
            'input[name=pipeline]' => array('value' => 'dompdf')
          )
        )
      );
      
      $form['conf']['dompdf']['filename'] = array(
        '#type' => 'textfield',
        '#title' => t('Filename'),
        '#required' => TRUE,
        '#description' => t('Set the filename of the generated PDF file. This file name will be the default name when downloading the file.'),
        '#default_value' => $settings['filename'] ? $settings['filename'] : 'file.pdf',
      );
      $form['conf']['dompdf']['attachment'] = array(
        '#type' => 'checkbox',
        '#title' => t('Attachment'),
        '#description' => t('Check if you want the force the browser to open a download dialog.'),
        '#default_value' => $settings['attachment'],
      );
      $form['conf']['dompdf']['debug_dompdf'] = array(
        '#type' => 'checkbox',
        '#title' => t('Debug dompdf'),
        '#description' => t('Renders a dompdf panel as normal html to see the result.'),
        '#default_value' => $settings['debug_dompdf'],
      );
      
      $form['conf']['dompdf']['save_pdf'] = array(
        '#type' => 'checkbox',
        '#title' => t('Save PDF file'),
        '#description' => t('Should the PDF file be saved on the server file?'),
        '#default_value' => $settings['save_pdf'],
      );
      $form['conf']['dompdf']['save_pdf_path'] = array(
        '#type' => 'textfield',
        '#title' => t('PDF filepath'),
        '#description' => t('Where should the PDF file should be saved? The path should start with a file stream wrapper like public://, private:// or temporary://. By default public:// will be used.'),
        '#default_value' => $settings['save_pdf_path'],
        '#states' => array(
          'visible' => array(
            'input[name=save_pdf]' => array('checked' => TRUE)
          )
        )
      );
        
      $form['conf']['dompdf']['save_html'] = array(
        '#type' => 'checkbox',
        '#title' => t('Save HTML file'),
        '#description' => t('Should the HTML file be saved on the server file?'),
        '#default_value' => $settings['save_html'],
      );
      $form['conf']['dompdf']['save_html_path'] = array(
        '#type' => 'textfield',
        '#title' => t('HTML filepath'),
        '#description' => t('Where should the HTML file should be saved? The path should start with a file stream wrapper like public://, private:// or temporary://. By default public:// will be used.'),
        '#default_value' => $settings['save_html_path'],
        '#states' => array(
          'visible' => array(
            'input[name=save_html]' => array('checked' => TRUE)
          )
        )
      );
      
      
      $form['conf']['dompdf']['css_files'] = array(
        '#type' => 'textarea',
        '#title' => t('Inline CSS files'),
        '#description' => t('One CSS file per line.'),
        '#default_value' => $settings['css_files'],
        '#states' => array(
          'visible' => array(
            'input[name=pipeline]' => array('value' => 'dompdf')
          )
        )
      );
      
      $form['conf']['dompdf']['filename']['#description'] .= ' ' . t(' You may use substitutions in filename.');
      $form['conf']['dompdf']['css_files']['#description'] .= ' ' . t(' You may use substitutions in CSS files list.');
      $form['conf']['dompdf']['css_files']['#description'] .= ' ' . t('You can use "[theme]" placeholder to get path to current page theme.');
      $form['conf']['dompdf']['css_files']['#description'] .= ' ' . t('You can use "[module-{module_name}]" or "[theme-{theme_name}]" placeholders to get path to module/theme.');
      
      if(module_exists('panels_css_js')) {
        $form['conf']['dompdf']['css_files']['#description'] .= ' ' . t('CSS files provided by the module !panels_css_js will be included too.', array(
          '!panels_css_js' => l('Panels CSS & JS', 'https://www.drupal.org/project/panels_css_js', array('external' => TRUE)),
        ));
      }
      
    }

  }
  
  $form['#submit'][] = 'panels_dompdf_form_panels_panel_context_edit_settings_submit';
}

function panels_dompdf_form_panels_panel_context_edit_settings_submit(&$form, &$form_state){
  if($form_state['values']['pipeline'] == 'dompdf'){
    $form_state['handler']->conf['filename'] = trim($form_state['values']['filename']);
    $form_state['handler']->conf['attachment'] = trim($form_state['values']['attachment']);
    $form_state['handler']->conf['debug_dompdf'] = trim($form_state['values']['debug_dompdf']);
    $form_state['handler']->conf['filename'] = trim($form_state['values']['filename']);
    
    if($form_state['handler']->conf['save_pdf']){
    }
    
    $form_state['handler']->conf['save_html'] = trim($form_state['values']['save_html']);
    if($form_state['handler']->conf['save_html']){
      $form_state['handler']->conf['save_html_path'] = trim($form_state['values']['save_html_path']);
    }
    
    $form_state['handler']->conf['css_files'] = trim($form_state['values']['css_files']);
    
    foreach (array('save_pdf' => 'save_pdf_path', 'save_html' => 'save_html_path') as $key => $value) {
      $form_state['handler']->conf[$key] = trim($form_state['values'][$key]);
      if($form_state['handler']->conf[$key]) {
        $path = trim($form_state['values'][$value]);
        if(!preg_match('/^.+:\/\//', $path)){
          $path = 'public://'.$path;
        }
        $form_state['handler']->conf[$value] = $path;
      }
    }
    
  }
}


function panels_dompdf_inline_php(Canvas &$pdf, $subtaks, $vars){
  drupal_alter('panels_dompdf_canvas', $pdf, $subtaks, $vars);
}
