<?php
use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * Renderer class for all dompdf behavior.
 */
class panels_renderer_dompdf extends panels_renderer_standard {
  
  function get_handler() {
    $pieces = explode(':', $this->display->cache_key);
    $machine_name = $pieces[3];
    $handlers = ctools_export_load_object('page_manager_handlers', 'names', array($machine_name));
    
    if(empty($handlers)){
      list($tast_id, $subtask_id) = explode('-', $pieces[1]);
      $this->tast = $tast_id;
      $this->subtask = $subtask_id;
      $task = page_manager_get_task($tast_id);
      $handlers = page_manager_load_task_handlers($task, $subtask_id);
    }
    
    return $handlers[$machine_name];
  }
  
  function render() {
    $this->meta_location = 'inline';
    $conf = $this->conf = $this->get_handler()->conf;
    $css_files = ctools_context_keyword_substitute($this->conf['css_files'], array(), $this->display->context);
    $css_files = preg_split('/\r\n|[\r\n]/', $css_files);
    
    foreach ($css_files as &$css_file) {
      $theme_path = dirname($GLOBALS['theme_info']->filename);
      $css_file = str_replace('[theme]', $theme_path, $css_file);
      
      $matches = array();
      $types = array('module', 'theme');
      
      foreach ($types as $type) {
        $pattern = '/\[' . $type . '-(.+)\]/';
      
        preg_match($pattern, $css_file, $matches);
      
        if (count($matches) > 1 && ($path = drupal_get_path($type, $matches[1])) != '') {
          $css_file = str_replace($matches[0], $path, $css_file);
        }
      }
    }
    
    if(module_exists('panels_css_js')){
      $include_files = panels_css_js_files_filter_value($conf['panels_css_js_files_css']);
      $css_files = array_merge($css_files, $include_files);
    }
    
    $css_files = array_filter($css_files);
    $css_files = array_unique($css_files);
    
    $output = '<style type="text/css">';
    foreach ($css_files as $css_file) {
      if(!empty($css_file) && file_exists($css_file)){
        $output .= file_get_contents($css_file);
      }
    }
    $output .= '</style>';
    
    $output .= parent::render();
    
    $inline_php = '<script type="text/php">panels_dompdf_inline_php($pdf, "' . $this->subtask . '", get_defined_vars());</script>';
    $output .= $inline_php;
    
    if($this->conf['debug_dompdf']){
      print $output;
    }
    else {
      if(is_array($this->display->context)){
        $filename = ctools_context_keyword_substitute($this->conf['filename'], array(), $this->display->context);
      }
      
      $this->pdf_output($output, $filename, $this->conf['attachment']);
    }
    drupal_exit();
  }
  
  /**
   * Transforms HTML to PDF and outputs it to the browser.
   */
  function pdf_output($html, $filename, $attachment = FALSE) {
    global $base_url, $base_root;
    $conf = $this->conf;
    
    // paths have to be relative else DomPDF will not able to find them
    $html = str_replace('src="/', 'src="'.$base_url.'/', $html);
    $html = str_replace('href="/', 'href="'.$base_root.'/', $html);
    
    $filename = str_replace('.pdf', '', $filename);
    $filename_token = $filename.'_'.hash('crc32', drupal_get_token($filename));
    
    if($conf['save_html']){
      $uri = ctools_context_keyword_substitute($conf['save_html_path'], array(), $this->display->context);
      $uri = token_replace($uri);
      $dir = dirname($uri);
      file_prepare_directory($dir, FILE_CREATE_DIRECTORY);
      file_put_contents($uri, $html);
    }
    
    $dompdf = $this->prepare_dompdf($html, $filename);

    if ($dompdf) {
      $output = $dompdf->output();
      
      if($conf['save_pdf']){
        $uri = ctools_context_keyword_substitute($conf['save_pdf_path'], array(), $this->display->context);
        $uri = token_replace($uri);
        $dir = dirname($uri);
        file_prepare_directory($dir, FILE_CREATE_DIRECTORY);
        file_put_contents($uri, $output);
      }
      
      $dompdf->stream($filename, array('Attachment' => $attachment));
    }
    else {
      return t('DOMPDF not found');
    }
    
  }
  
  /**
   * Helper function that instantiates new DOMPDF class and renders the HTML.
   *
   * The returned dompdf object can then be used to either directly stream the
   * output or to create a file.
   */
  function prepare_dompdf($html, $filename) {
    $path = libraries_get_path('dompdf');
    if ($path) {
      // dompdf needs write access to its font directory.
      // Copy "libraries/dompdf/lib/fonts" to your public files directory in order
      // for this to work.
      require_once DRUPAL_ROOT . '/' . $path . '/autoload.inc.php';
      
      define('DOMPDF_ENABLE_CSS_FLOAT', TRUE);
      
      $dompdf = new \Dompdf\Dompdf();
           
      $options = new \Dompdf\Options();
      $options->setIsRemoteEnabled(TRUE);
      $options->setFontDir(drupal_realpath('public://fonts'));
      $options->setTempDir(drupal_realpath('temporary://dompdf'));
      $options->setDefaultFont('DejaVuSans');
      $options->setIsHtml5ParserEnabled(TRUE);
      $options->setIsJavascriptEnabled(TRUE);
      $options->setIsPhpEnabled(TRUE);

      $dompdf->setOptions($options);
      $dompdf->setPaper('a4', 'portrait');
  
      $html = htmlspecialchars_decode(htmlentities($html, ENT_NOQUOTES, 'UTF-8'), ENT_NOQUOTES);
      $dompdf->loadHtml($html, 'UTF-8');
      $dompdf->render();
      return $dompdf;
    }
    return FALSE;
  }
  
}
