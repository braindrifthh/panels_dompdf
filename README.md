# Panels Dompdf #

This Drupal 7 module implements a panel renderer that renders a custom page as PDF using [dompdf](https://github.com/dompdf/dompdf).